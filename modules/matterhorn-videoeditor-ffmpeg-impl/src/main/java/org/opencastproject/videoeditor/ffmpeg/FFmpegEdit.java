/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */

package org.opencastproject.videoeditor.ffmpeg;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import org.apache.commons.lang.StringUtils;

import org.opencastproject.util.IoSupport;
import org.opencastproject.videoeditor.api.ProcessFailedException;
import org.opencastproject.videoeditor.silencedetection.api.MediaSegment;
import org.opencastproject.videoeditor.silencedetection.api.MediaSegments;
import org.opencastproject.videoeditor.silencedetection.impl.SilenceDetectionProperties;
import org.opencastproject.videoeditor.impl.VideoClip;
import org.opencastproject.videoeditor.impl.VideoEditorProperties;

import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * FFmpeg wrappers:
 * processEdits:    process SMIL definitions of segments into one consecutive video
 *                  There is a fade in and a fade out at the beginning and end of each clip
 *                  
 * runSilenceDetection: Find and return a list of non-silent sections.                 
 *
 */
public class FFmpegEdit {

  private static final Logger logger = LoggerFactory.getLogger(FFmpegEdit.class);
  private static final String FFMPEG_BINARY_DEFAULT = "ffmpeg";
  private static final String CONFIG_FFMPEG_PATH = "org.opencastproject.composer.ffmpegpath";

  private static final String DEFAULT_FFMPEG_PROPERTIES = "-strict -2 -preset fast -qp 0";
  public static final String DEFAULT_OUTPUT_FILE_EXTENSION = ".mp4";
  private static final String DEFAULT_AUDIO_FADE = "2.0";
  private static final String DEFAULT_VIDEO_FADE = "2.0";
  private static String binary = FFMPEG_BINARY_DEFAULT;

  private static final String DEFAULT_SILENCE_MIN_LENGTH = "5000"; // In milliseconds
  private static final String DEFAULT_SILENCE_PRE_LENGTH = "2000";
  private static final String DEFAULT_THRESHOLD_DB = "-30";
  private static final String DEFAULT_VOICE_MIN_LENGTH = "60000";
  protected float vfade;
  protected float afade;
  protected String ffmpegProperties = DEFAULT_FFMPEG_PROPERTIES;
  protected String videoCodec = null;  // By default, use the same codec as source
  protected String audioCodec = null;

  public static void init(BundleContext bundleContext) {
    String path = bundleContext.getProperty(CONFIG_FFMPEG_PATH);

    if (path != null) {
      binary = path; 
    }
  }

  public FFmpegEdit()
  {  
    this.afade = Float.parseFloat(DEFAULT_AUDIO_FADE);   
    this.vfade = Float.parseFloat(DEFAULT_VIDEO_FADE);  
    this.ffmpegProperties = DEFAULT_FFMPEG_PROPERTIES;
  }

  public FFmpegEdit(Properties properties)
  {
    String fade = properties.getProperty(VideoEditorProperties.AUDIO_FADE, DEFAULT_AUDIO_FADE);
    try {
      this.afade = Float.parseFloat(fade);
    } catch (Exception e) {
      this.afade = Float.parseFloat(DEFAULT_AUDIO_FADE);
    }
    fade = properties.getProperty(VideoEditorProperties.VIDEO_FADE, DEFAULT_VIDEO_FADE);
    try {
      this.vfade = Float.parseFloat(fade);
    } catch (Exception e) {
      this.vfade = Float.parseFloat(DEFAULT_VIDEO_FADE);
    }
    this.ffmpegProperties = properties.getProperty(VideoEditorProperties.FFMPEG_PROPERTIES, DEFAULT_FFMPEG_PROPERTIES);
    this.videoCodec = properties.getProperty(
            VideoEditorProperties.VIDEO_CODEC, null);
    this.audioCodec = properties.getProperty(
            VideoEditorProperties.AUDIO_CODEC, null);
  }


  public String processEdits(List<String> inputfiles, String dest, List<VideoClip> cleanclips)
          throws Exception {
    List<String> cmd = makeEdits(inputfiles, dest, cleanclips);	
    return run(cmd);
  }

  // Run the ffmpeg command with the params
  // Takes a list of words as params, the output is logged
  private String run(List<String> params) {
    BufferedReader in = null;
    Process encoderProcess = null;
    try {
      params.add(0, binary);
      logger.info("executing command: " + StringUtils.join(params, " "));
      ProcessBuilder pbuilder = new ProcessBuilder(params);
      pbuilder.redirectErrorStream(true); 
      encoderProcess = pbuilder.start();
      in = new BufferedReader(new InputStreamReader(
              encoderProcess.getInputStream()));
      String line;
      int n = 5;
      while ((line = in.readLine()) != null) {
	  if (n-- > 0)
		  logger.info(line);
      }

      // wait until the task is finished
      encoderProcess.waitFor();
      int exitCode = encoderProcess.exitValue();
      if (exitCode != 0) {       
        throw new Exception("Ffmpeg exited abnormally with status " + exitCode);
      }

    } catch (Exception ex) {
      logger.error("VideoEditor ffmpeg failed", ex);
      return ex.toString();
    } finally {
      IoSupport.closeQuietly(in);
      IoSupport.closeQuietly(encoderProcess);
    }
    return null;
  }

  // Create the ffmpeg command from in-out points
  public List<String> makeEdits(List<String> inputfiles, String dest,
          List<VideoClip> clips) throws Exception {

    DecimalFormat f = new DecimalFormat("0.00");
    int n = clips.size();
    String outmap = "";
    List<String> command = new ArrayList<String>();
    List<String> vpads = new ArrayList<String>();
    List<String> apads = new ArrayList<String>();
    List<String> clauses = new ArrayList<String>(); // The clauses are ordered

    if (n > 1) { // Create the input pads if we have multiple segments
      for (int i = 0; i < n ; i++) {
        vpads.add("[v" + i + "]");  // post filter
        apads.add("[a" + i + "]");
      }
    }
    int i;

    for (i = 0; i < n; i++) { // Each clip
      // get clip and add fades to each clip
      VideoClip vclip = clips.get(i);
      int fileindx = vclip.getSrc();   // get source file by index
      double inpt = vclip.getStart();     // get in points
      double duration = vclip.getDuration();
      double vend = duration - vfade;
      double aend = duration - afade;
      String clip;
      clip = "[" + fileindx + ":v]trim=" + f.format(inpt)
                + ":duration=" + f.format(duration)
                + ",setpts=PTS-STARTPTS,fade=t=in:st=0:d=" + vfade
                + ",fade=t=out:st=" + f.format(vend) + ":d=" + vfade + "[v"
                + i + "]";

      clauses.add(clip);
      clip = "[" + fileindx + ":a]atrim=" + f.format(inpt) + ":duration="
                + f.format(duration)
                + ",asetpts=PTS-STARTPTS,afade=t=in:st=0:d=" + afade
                + ",afade=t=out:st=" + f.format(aend) + ":d=" + afade + "[a"
                + i + "]";
      clauses.add(clip);
    }
    if (n > 1) { // concat the outpads when there are more then 1 per stream
                  // use unsafe because different files may have different SAR/framerate
      clauses.add(StringUtils.join(vpads, "") + "concat=n=" + n + ":unsafe=1[ov0]"); // concat video clips      
      clauses.add(StringUtils.join(apads, "") + "concat=n=" + n
                + ":v=0:a=1[oa0]"); // concat audio clips in stream 0, video in stream 1
      outmap = "o";                 // if more than one clip
    }
    command.add("-y");      // overwrite old pathname
    for (String o : inputfiles) {
      command.add("-i");   // Add inputfile in the order of entry
      command.add(o);
    }
    command.add("-filter_complex");
    command.add(StringUtils.join(clauses, ";"));
    String[] options = ffmpegProperties.split(" ");
    command.addAll(Arrays.asList(options));
    command.add("-map");
    command.add("[" + outmap + "a0]");
    command.add("-map");
    command.add("[" + outmap + "v0]");
    if (videoCodec != null) { // If using different codecs from source, add them here
      command.add("-c:v");
      command.add(videoCodec);
    }
    if (audioCodec != null) {
      command.add("-c:a");
      command.add(audioCodec);
    }
    command.add(dest);

    return command;
  }

  // Clean up the edit points, make sure they are at least 2 seconds apart
  // Otherwise it can be very slow to run and output will be ugly because of the fades
  private static List<Double> sortSegments(List<Double> edits) {
    LinkedList<Double> ll = new LinkedList<Double>();
    List<Double> clips = new ArrayList<Double>();

    Iterator<Double> it = edits.iterator();
    while (it.hasNext()) { // Check for legal durations
      double inpt = it.next();
      double outpt = it.next();
      if (outpt - inpt > 2) { // Keep segments at least 2 seconds long
        ll.add(inpt);
        ll.add(outpt);
      }
    }
    double inpt = ll.pop(); // initialize
    double outpt = ll.pop();
    while (!ll.isEmpty()) { // Check that 2 segments are at least 2 sec apart
      if (ll.peek() != null) {
        if ((ll.peek() - outpt) < 2) { // collapse two segments into one
          ll.pop();
          outpt = ll.pop();
        } else {
          clips.add(inpt);
          clips.add(outpt);
          inpt = ll.pop(); // onto the next segment
          outpt = ll.pop();
        }
      } // add last segment
    }
    clips.add(inpt);
    clips.add(outpt);
    System.out.println(" clips segments " + clips.toString());
    return clips;
  }

  // This method returns the non-silent sections
  public MediaSegments runSilenceDetection(Properties properties,
          String trackId, String filePath) throws ProcessFailedException {	  
    // in milliseconds
    long minSilenceLength = Long.parseLong(properties.getProperty(
            SilenceDetectionProperties.SILENCE_MIN_LENGTH,
            DEFAULT_SILENCE_MIN_LENGTH));
    long minVoiceLength =  Long.parseLong(properties.getProperty(
            SilenceDetectionProperties.VOICE_MIN_LENGTH,
            DEFAULT_VOICE_MIN_LENGTH));		
    long preSilenceLength =  Long.parseLong(properties.getProperty(
            SilenceDetectionProperties.SILENCE_PRE_LENGTH,
            DEFAULT_SILENCE_PRE_LENGTH));
    double thresholdDB = Double.parseDouble(properties.getProperty(
            SilenceDetectionProperties.SILENCE_THRESHOLD_DB,
            DEFAULT_THRESHOLD_DB));
    
    double mediaLength = 0; 
    Pattern durationpatt = Pattern.compile("Duration:\\s*([:\\.\\d]+)");
    Pattern silpatt = Pattern
            .compile("silence_(start|end):\\s*([-\\.\\d]+)");
    LinkedList<MediaSegment> segments = new LinkedList<MediaSegment>();
    List<String> command = new ArrayList<String>();
    command.add("-i");
    command.add(filePath);
    command.add("-af");
    command.add("silencedetect=n=" + thresholdDB + "dB:d=" + (0.001 * minSilenceLength));
    command.add("-f");
    command.add("null");
    command.add("-");
    int dummy = 10; // fudge factor - when the segments go from 0 to the actual length, the SMIL file from the UI goes out of bounds
    try {
      command.add(0, binary);
      logger.info("SilenceDetection ffmpeg: {}", StringUtils.join(command," "));

      ProcessBuilder pbuilder = new ProcessBuilder(command);
      pbuilder.redirectErrorStream(true); // To read info on the media
      Process encoderProcess = pbuilder.start();

      BufferedReader in = new BufferedReader(new InputStreamReader(
              encoderProcess.getInputStream()));
      String line;

   // Add dummy fudge factor to line up segments later  
      double lastSilenceStop = dummy;
      double thisSilenceStart = dummy;
      double thisSilenceStop = dummy;
      boolean hasAudio = false;
      while ((line = in.readLine()) != null) {
        line = line.trim();

        if (mediaLength == 0) { // Find MediaLength
          Matcher durationMatch = durationpatt.matcher(line);
          if (durationMatch.find()) {
            String[] values = durationMatch.group(1).split(":"); // Can't use dateformat with subseconds
            for (String s : values) {
              mediaLength = mediaLength * 60 + Double.parseDouble(s);
            }
            mediaLength =  mediaLength * 1000; // as ms
            continue;
          }
        }
        // Make sure that we detect an audio stream
        if (!hasAudio && line.matches("(?:Stream\\s+[^A-Z]+\\s+Audio).*")) { hasAudio = true; }
        // Look for Silent segments
        Matcher m = silpatt.matcher(line);
        if (m.find()) {    
          // logger.debug("Silencedetect In [" + line + "]");
          if ("start".equals(m.group(1))) {
            thisSilenceStart = 1000 * Double.parseDouble(m.group(2)); // in ms             
            if ((thisSilenceStart - lastSilenceStop) > preSilenceLength) {
              MediaSegment ms = (new MediaSegment((long) lastSilenceStop, (long)thisSilenceStart));
              segments.add(ms);
              logger.debug("Silence [" + ms.getSegmentStart() + "," + ms.getSegmentStop() + "]");
            }
          } 
          else if ("end".equals(m.group(1))) {   
            thisSilenceStop = 1000 * Double.parseDouble(m.group(2));        
            if (((thisSilenceStop - thisSilenceStart) > minSilenceLength)) {
              lastSilenceStop = thisSilenceStop;     // Log this silence
            }   
          }
        }
      }

      if (!hasAudio) throw new ProcessFailedException("This media has no audio track");  
      segments.add(new MediaSegment((long)lastSilenceStop,(long) mediaLength - dummy)); 

      // wait until the task is finished
      encoderProcess.waitFor();
      int exitCode = encoderProcess.exitValue();
      if (exitCode != 0) {
        throw new ProcessFailedException("SilenceDetection exited abnormally with status "
                + exitCode);
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
      throw new ProcessFailedException("SilenceDetection Interrupted " + e.toString());
    } catch (IOException e) {
      e.printStackTrace();
      throw new ProcessFailedException("SilenceDetection IO Exception " + e.toString());
    }
    List<MediaSegment> segmentsTmp = segments;

    segments = new LinkedList<MediaSegment>();
    for (MediaSegment segment : segmentsTmp) {
      logger.debug("[" + segment.getSegmentStart() + "," + segment.getSegmentStop() + "] >" + minVoiceLength);	  
      if (segment.getSegmentStop() - segment.getSegmentStart() >= minVoiceLength) {
        segments.add(segment);
      }
    }
    
    if (segments.size() == 0 && segmentsTmp.size() != 0) {
      logger.warn("Found segments are shorter than minimum segment length. Join them all...");
      segments.add(new MediaSegment(segmentsTmp.get(0).getSegmentStart(),
              segmentsTmp.get(segmentsTmp.size() - 1).getSegmentStop()));
    }
   
    return new MediaSegments(trackId, filePath, segments);
  }
}
