package edu.harvard.dce.operation.handler;

import org.opencastproject.job.api.JobContext;
import org.opencastproject.util.NotFoundException;
import org.opencastproject.workflow.api.WorkflowDatabaseException;
import org.opencastproject.workflow.api.WorkflowDefinition;
import org.opencastproject.workflow.api.WorkflowInstance;
import org.opencastproject.workflow.api.WorkflowOperationException;
import org.opencastproject.workflow.api.WorkflowOperationResult;
import org.opencastproject.workflow.api.WorkflowOperationResult.Action;
import org.opencastproject.workflow.api.WorkflowService;
import org.opencastproject.workflow.handler.ResumableWorkflowOperationHandlerBase;

import org.apache.commons.lang.StringUtils;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class TestLocalWorkflowOperationHandler extends ResumableWorkflowOperationHandlerBase {

  private static final Logger logger = LoggerFactory.getLogger(TestLocalWorkflowOperationHandler.class);

  /** Path to the hold ui resources */
  private static final String HOLD_UI_PATH = "/ui/operation/test-local/index-test-local.html";

  /** Operation configuration options **/
  public final static String CONTINUE_WORKFLOW_DEFINITION = "continueWorkflow";
  public final static String RESTART_WORKFLOW_DEFINITION = "restartWorkflow";

  /** Workflow configuration value for future appending of workflow */
  public static final String OPT_WORKFLOW = "workflowSelector";

  /** The workflow service */
  private WorkflowService workflowService;

  public void activate(ComponentContext cc) {
    super.activate(cc);
    setHoldActionTitle("Test Local");

    // Register the supported configuration options
    addConfigurationOption(CONTINUE_WORKFLOW_DEFINITION,
            "Workflow definition identifier if continue action is selected");
    addConfigurationOption(RESTART_WORKFLOW_DEFINITION, "Workflow definition identifier if restart action is selected");

    registerHoldStateUserInterface(HOLD_UI_PATH);
    logger.info("Registering test local hold state ui from classpath {}", HOLD_UI_PATH);
  }

  /**
   * {@inheritDoc}
   * 
   * @see org.opencastproject.workflow.api.WorkflowOperationHandler#start(org.opencastproject.workflow.api.WorkflowInstance,
   *      JobContext)
   */
  @Override
  public WorkflowOperationResult start(WorkflowInstance workflowInstance, JobContext context)
          throws WorkflowOperationException {
    logger.info("Holding for test local...");

    return createResult(Action.PAUSE);
  }

  /**
   * {@inheritDoc}
   * 
   * @see org.opencastproject.workflow.api.AbstractWorkflowOperationHandler#skip(org.opencastproject.workflow.api.WorkflowInstance,
   *      JobContext)
   */
  @Override
  public WorkflowOperationResult skip(WorkflowInstance workflowInstance, JobContext context)
          throws WorkflowOperationException {
    return super.skip(workflowInstance, context);
  }

  /**
   * {@inheritDoc}
   * 
   * @see org.opencastproject.workflow.api.ResumableWorkflowOperationHandler#resume(org.opencastproject.workflow.api.WorkflowInstance,
   *      JobContext, java.util.Map)
   */
  @Override
  public WorkflowOperationResult resume(WorkflowInstance workflowInstance, JobContext context,
          Map<String, String> properties) throws WorkflowOperationException {
    try {
      logger.info("Resuming Test Local operation for workflow {} using {}", workflowInstance.getId(), properties);

      String action = properties.get("action");
      String wfDefId = null;
      if ("continue".equals(action)) {
        wfDefId = workflowInstance.getCurrentOperation().getConfiguration(CONTINUE_WORKFLOW_DEFINITION);
      } else if ("restart".equals(action)) {
        wfDefId = workflowInstance.getCurrentOperation().getConfiguration(RESTART_WORKFLOW_DEFINITION);
      } else if ("discard".equals(action)) {
        logger.info("Workflow {} was discarded by user", workflowInstance.getId());
        throw new WorkflowOperationException("Workflow " + workflowInstance.getId() + " was discarded by user");
      } else {
        logger.info("Workflow {}, unknown action {}", workflowInstance.getId(), action);
        throw new WorkflowOperationException("Workflow " + workflowInstance.getId() + ", unknown action: " + action);
      }

      if (appendWorkflow(workflowInstance, wfDefId)) {
        return super.resume(workflowInstance, context, properties);
      } else {
        return createResult(Action.PAUSE);
      }
    } catch (RuntimeException e) {
      logger.error(e.getClass().toString() + ":" + e.getMessage());
      throw new WorkflowOperationException(e);
    }
  }

  protected boolean appendWorkflow(WorkflowInstance workflowInstance, String workflowDefinitionId) {

    if (StringUtils.isBlank(workflowDefinitionId))
      return false;

    logger.info("Workflow {}: appending workflow {}", workflowInstance.getId(), workflowDefinitionId);

    try {
      WorkflowDefinition definition = workflowService.getWorkflowDefinitionById(workflowDefinitionId);

      if (definition != null) {
        workflowInstance.extend(definition);
        return true;
      }
    } catch (WorkflowDatabaseException e) {
      logger.warn("Error querying workflow service for '{}'", workflowDefinitionId, e);
    } catch (NotFoundException e) {
      logger.warn("Workflow '{}' not found. Entering hold state to resolve", workflowDefinitionId);
    }

    logger.info("Entering hold state to ask for workflow");
    return false;
  }

  /**
   * Sets the workflow service
   * 
   * @param wfService
   *          the workflow service
   */
  public void setWorkflowService(WorkflowService wfServicce) {
    this.workflowService = wfServicce;
  }
}
