package edu.harvard.dce.operation.handler;

import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.mediapackage.MediaPackageBuilder;
import org.opencastproject.mediapackage.MediaPackageBuilderFactory;
import org.opencastproject.workflow.api.WorkflowDefinition;
import org.opencastproject.workflow.api.WorkflowInstance.WorkflowState;
import org.opencastproject.workflow.api.WorkflowInstanceImpl;
import org.opencastproject.workflow.api.WorkflowOperationException;
import org.opencastproject.workflow.api.WorkflowOperationInstance;
import org.opencastproject.workflow.api.WorkflowOperationInstance.OperationState;
import org.opencastproject.workflow.api.WorkflowOperationInstanceImpl;
import org.opencastproject.workflow.api.WorkflowOperationResult;
import org.opencastproject.workflow.api.WorkflowOperationResult.Action;
import org.opencastproject.workflow.api.WorkflowParser;
import org.opencastproject.workflow.api.WorkflowService;

import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestLocalWorkflowOperationHandlerTest {

  private MediaPackage mp;
  private WorkflowInstanceImpl workflowInstance;
  private TestLocalWorkflowOperationHandler operationHandler;
  private WorkflowService workflowService;

  private static final String WF_CONTINUE = "wf_continue.xml";
  private static final String WF_RESTART = "wf_restart.xml";

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Before
  public void setUp() throws Exception {
    // SetUp() runs before each test
    MediaPackageBuilder builder = MediaPackageBuilderFactory.newInstance().newMediaPackageBuilder();

    // Test resources
    URI uriMP = TestLocalWorkflowOperationHandlerTest.class.getResource("/mp/manifest.xml").toURI();
    mp = builder.loadFromXml(uriMP.toURL().openStream());
    URI uriWf1 = TestLocalWorkflowOperationHandlerTest.class.getResource("/" + WF_CONTINUE).toURI();
    WorkflowDefinition defContinue = WorkflowParser.parseWorkflowDefinition(uriWf1.toURL().openStream());
    URI uriWf2 = TestLocalWorkflowOperationHandlerTest.class.getResource("/" + WF_RESTART).toURI();
    WorkflowDefinition defRestart = WorkflowParser.parseWorkflowDefinition(uriWf2.toURL().openStream());

    // Mock-up of workflow service
    workflowService = EasyMock.createNiceMock(WorkflowService.class);
    EasyMock.expect(workflowService.getWorkflowDefinitionById(WF_CONTINUE)).andReturn(defContinue);
    EasyMock.expect(workflowService.getWorkflowDefinitionById(WF_RESTART)).andReturn(defRestart);
    EasyMock.replay(workflowService);

    // Operation handler to be tested
    operationHandler = new TestLocalWorkflowOperationHandler();
    operationHandler.setWorkflowService(workflowService);

    workflowInstance = new WorkflowInstanceImpl();
    workflowInstance.setId(1);
    workflowInstance.setState(WorkflowState.RUNNING);
    workflowInstance.setMediaPackage(mp);

    WorkflowOperationInstanceImpl operation = new WorkflowOperationInstanceImpl("test-local", OperationState.RUNNING);
    operation.setConfiguration(TestLocalWorkflowOperationHandler.CONTINUE_WORKFLOW_DEFINITION, WF_CONTINUE);
    operation.setConfiguration(TestLocalWorkflowOperationHandler.RESTART_WORKFLOW_DEFINITION, WF_RESTART);

    List<WorkflowOperationInstance> operationsList = new ArrayList<WorkflowOperationInstance>();
    operationsList.add(operation);
    workflowInstance.setOperations(operationsList);
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testResumeWithContinue() throws WorkflowOperationException {
    Map<String, String> props = new HashMap<String, String>();
    props.put("action", "continue");

    // Run the media package through the operation handler, ensuring that the
    // proper workflow got appended
    WorkflowOperationResult result = operationHandler.resume(workflowInstance, null, props);
    Assert.assertEquals(Action.CONTINUE, result.getAction());

    List<WorkflowOperationInstance> ops = workflowInstance.getOperations();
    Assert.assertEquals(2, ops.size());

    Assert.assertEquals("continue1", ops.get(1).getTemplate());
  }

  @Test
  public void testResumeWithRestart() throws WorkflowOperationException {
    Map<String, String> props = new HashMap<String, String>();
    props.put("action", "restart");

    // Run the media package through the operation handler, ensuring that the
    // proper workflow got appended
    WorkflowOperationResult result = operationHandler.resume(workflowInstance, null, props);
    Assert.assertEquals(Action.CONTINUE, result.getAction());

    List<WorkflowOperationInstance> ops = workflowInstance.getOperations();
    Assert.assertEquals(3, ops.size());

    Assert.assertEquals("restart1", ops.get(1).getTemplate());
    Assert.assertEquals("restart2", ops.get(2).getTemplate());
  }

  @Test
  public void testResumeWithInvalidWorkflowDefinition() throws WorkflowOperationException {
    // Set the operation configuration for this test with unknown workflow definitions
    WorkflowOperationInstance operationUnknownWf = workflowInstance.getOperations().get(0);
    operationUnknownWf.setConfiguration(TestLocalWorkflowOperationHandler.CONTINUE_WORKFLOW_DEFINITION, "Unknown");
    operationUnknownWf.setConfiguration(TestLocalWorkflowOperationHandler.RESTART_WORKFLOW_DEFINITION, "Unknown");

    Map<String, String> props = new HashMap<String, String>();
    props.put("action", "restart");

    // If workflow definition unknown, it should pause for user selection
    WorkflowOperationResult result = operationHandler.resume(workflowInstance, null, props);
    Assert.assertEquals(Action.PAUSE, result.getAction());
  }

  @Test(expected = WorkflowOperationException.class)
  public void testWorkflowOperationException() throws WorkflowOperationException {
    Map<String, String> props = new HashMap<String, String>();
    props.put("action", "unknown");

    WorkflowOperationResult result = operationHandler.resume(workflowInstance, null, props);
  }
}
