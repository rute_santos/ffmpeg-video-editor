/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package edu.harvard.dce.operation.handler;

import org.opencastproject.job.api.Job;
import org.opencastproject.job.api.JobContext;
import org.opencastproject.mediapackage.Catalog;
import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.mediapackage.MediaPackageElementFlavor;
import org.opencastproject.mediapackage.MediaPackageElementParser;
import org.opencastproject.mediapackage.MediaPackageException;
import org.opencastproject.mediapackage.Track;
import org.opencastproject.smil.api.SmilException;
import org.opencastproject.smil.api.SmilService;
import org.opencastproject.smil.entity.api.Smil;
import org.opencastproject.util.NotFoundException;
import org.opencastproject.videoeditor.api.ProcessFailedException;
import org.opencastproject.videoeditor.api.VideoEditorService;
import org.opencastproject.workflow.api.AbstractWorkflowOperationHandler;
import org.opencastproject.workflow.api.WorkflowInstance;
import org.opencastproject.workflow.api.WorkflowOperationException;
import org.opencastproject.workflow.api.WorkflowOperationInstance;
import org.opencastproject.workflow.api.WorkflowOperationResult;
import org.opencastproject.workflow.api.WorkflowOperationResult.Action;
import org.opencastproject.workspace.api.Workspace;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * The operation handler for the "process-smil" workflow operation. This code is based on the
 * VideoEditorWorkflowOperationHandler; the difference is that this operation assumes there's already a SMIL in the
 * media package that describes how the video(s) should be trimmed/edited so it doesn't have an UI associated and
 * doesn't pause.
 */

public class ProcessSmilWorkflowOperationHandler extends AbstractWorkflowOperationHandler {
  private static final Logger logger = LoggerFactory.getLogger(ProcessSmilWorkflowOperationHandler.class);
  /**
   * Name of the configuration option that provides the source flavors we are looking for
   */
  private static final String SOURCE_FLAVOR_PROPERTY = "source-flavors";
  /**
   * Name of the configuration option that provides the smil flavor
   */
  private static final String SMIL_FLAVOR_PROPERTY = "smil-flavor";
  /**
   * Name of the configuration that provides the target flavor subtype we will produce
   */
  private static final String TARGET_FLAVOR_SUBTYPE_PROPERTY = "target-flavor-subtype";

  /** The configuration options for this handler */
  private static final SortedMap<String, String> CONFIG_OPTIONS;
  static {
    CONFIG_OPTIONS = new TreeMap<String, String>();
    CONFIG_OPTIONS.put(SOURCE_FLAVOR_PROPERTY, "The flavor for work files (tracks to edit).");
    CONFIG_OPTIONS.put(SMIL_FLAVOR_PROPERTY, "The flavor for smil files.");
    CONFIG_OPTIONS.put(TARGET_FLAVOR_SUBTYPE_PROPERTY, "The flavor subtype for target (generated) files.");
  }

  /**
   * The Smil service to modify smil files.
   */
  private SmilService smilService;
  /**
   * The VideoEditor service to edit files.
   */
  private VideoEditorService videoEditorService;
  /**
   * The workspace.
   */
  private Workspace workspace;

  @Override
  public void activate(ComponentContext cc) {
    super.activate(cc);
  }

  /**
   * {@inheritDoc}
   * 
   * @see org.opencastproject.workflow.api.WorkflowOperationHandler#getConfigurationOptions()
   */
  @Override
  public SortedMap<String, String> getConfigurationOptions() {
    return CONFIG_OPTIONS;
  }

  /**
   * {@inheritDoc}
   * 
   * @see org.opencastproject.workflow.api.WorkflowOperationHandler#start(org.opencastproject.workflow.api.WorkflowInstance,
   *      JobContext)
   */
  @Override
  public WorkflowOperationResult start(WorkflowInstance workflowInstance, JobContext context)
          throws WorkflowOperationException {
    logger.info("VideoEdit workflow {} using SMIL Document", workflowInstance.getId());
    MediaPackage mp = workflowInstance.getMediaPackage();

    // get smil
    MediaPackageElementFlavor smilFlavor = MediaPackageElementFlavor.parseFlavor(workflowInstance.getCurrentOperation()
            .getConfiguration(SMIL_FLAVOR_PROPERTY));
    Catalog[] catalogs = mp.getCatalogs(smilFlavor);
    if (catalogs.length == 0) {
      throw new WorkflowOperationException("MediaPackage does not contain a SMIL document.");
    }

    // get all source tracks
    String configuredSourceFlavors = workflowInstance.getCurrentOperation().getConfiguration(SOURCE_FLAVOR_PROPERTY);
    List<Track> sourceTracksList = new LinkedList<Track>();
    for (String f : StringUtils.split(configuredSourceFlavors, ",")) {
      String sourceFlavorStr = StringUtils.trimToNull(f);
      if (sourceFlavorStr == null)
        continue;
      MediaPackageElementFlavor sourceFlavor = MediaPackageElementFlavor.parseFlavor(sourceFlavorStr);
      String sourceFlavorType = sourceFlavor.getType();
      // Support for presenter/source*, presentation/source*
      String sourceFlavorSubtype = sourceFlavor.getSubtype();
      boolean wildcard = false;
      if (sourceFlavorSubtype.endsWith("*")) {
        sourceFlavorSubtype = sourceFlavorSubtype.substring(0, sourceFlavorSubtype.length() - 1);
        wildcard = true;
      }
      for (Track t : mp.getTracks()) {
        if ((t.getFlavor().getType().equals(sourceFlavorType) || "*".equals(sourceFlavorType))
                && (t.getFlavor().getSubtype().equals(sourceFlavorSubtype) || "*".equals(sourceFlavorSubtype) || (wildcard && t
                        .getFlavor().getSubtype().startsWith(sourceFlavorSubtype)))) {
          sourceTracksList.add(t);
        }
      }
    }
    if (sourceTracksList.size() == 0)
      throw new WorkflowOperationException(String.format(
              "No source tracks found. Configuration property %s is not valid", SOURCE_FLAVOR_PROPERTY));

    // process smil
    File smilFile;
    Smil smil = null;
    try {
      smilFile = workspace.get(catalogs[0].getURI());
      smil = smilService.fromXml(smilFile).getSmil();
    } catch (NotFoundException ex) {
      throw new WorkflowOperationException("MediaPackage does not contain a smil catalog.");
    } catch (IOException ex) {
      throw new WorkflowOperationException("Failed to read smil catalog.", ex);
    } catch (SmilException ex) {
      throw new WorkflowOperationException(ex);
    }

    // create video edit jobs and run them
    List<Job> jobs = null;
    try {
      logger.info("Create processing jobs for smil " + smil.getId());
      jobs = videoEditorService.processSmil(smil);
      if (!waitForStatus(jobs.toArray(new Job[jobs.size()])).isSuccess()) {
        throw new WorkflowOperationException("Smil processing jobs for smil " + smil.getId()
                + " are ended unsuccessfull.");
      }
      logger.info("Smil " + smil.getId() + " processing finished.");
    } catch (ProcessFailedException ex) {
      throw new WorkflowOperationException("Processing smil " + smil.getId() + " failed", ex);
    }

    String targetFlavorSubType = workflowInstance.getCurrentOperation()
            .getConfiguration(TARGET_FLAVOR_SUBTYPE_PROPERTY);

    // move edited tracks to work location and set target flavor
    Track sourceTrack;
    Track editedTrack;
    for (Job job : jobs) {
      try {
        editedTrack = (Track) MediaPackageElementParser.getFromXml(job.getPayload());
        MediaPackageElementFlavor editedTrackFlavor = editedTrack.getFlavor();
        sourceTrack = null;
        for (Track track : sourceTracksList) {
          if (track.getFlavor().getType().equals(editedTrackFlavor.getType())) {
            sourceTrack = track;
            break;
          }
        }
        if (sourceTrack == null) {
          throw new WorkflowOperationException(String.format("MediaPackage does not contain track with %s flavor.",
                  new MediaPackageElementFlavor(editedTrack.getFlavor().getType(), targetFlavorSubType).toString()));
        }

        String newTrackFileName = String.format("%s-%s.%s", editedTrackFlavor.getType(), targetFlavorSubType,
                FilenameUtils.getExtension(editedTrack.getURI().getPath().toString()));
        URI newUri = workspace.moveTo(editedTrack.getURI(), mp.getIdentifier().compact(), editedTrack.getIdentifier(),
                newTrackFileName);
        editedTrack.setURI(newUri);
        editedTrack.setFlavor(new MediaPackageElementFlavor(editedTrack.getFlavor().getType(), targetFlavorSubType));
        mp.addDerived(editedTrack, sourceTrack);
      } catch (MediaPackageException ex) {
        throw new WorkflowOperationException("Failed to get edited track information.", ex);
      } catch (NotFoundException ex) {
        throw new WorkflowOperationException("Moving edited track to work location failed.", ex);
      } catch (IOException ex) {
        throw new WorkflowOperationException("Moving edited track to work location failed.", ex);
      } catch (IllegalArgumentException ex) {
        throw new WorkflowOperationException("Moving edited track to work location failed.", ex);
      }
    }

    logger.info("VideoEdit workflow {} finished", workflowInstance.getId());

    return createResult(mp, Action.CONTINUE);
  }

  /**
   * {@inheritDoc}
   * 
   * @see org.opencastproject.workflow.api.AbstractWorkflowOperationHandler#skip(org.opencastproject.workflow.api.WorkflowInstance,
   *      JobContext)
   */
  @Override
  public WorkflowOperationResult skip(WorkflowInstance workflowInstance, JobContext context)
          throws WorkflowOperationException {
    // We still need to put tracks in the mediapackage with the target flavor
    MediaPackage mp = workflowInstance.getMediaPackage();
    WorkflowOperationInstance currentOperation = workflowInstance.getCurrentOperation();

    // get target flavor subtype
    String configuredTargetFlavorSubtype = currentOperation.getConfiguration(TARGET_FLAVOR_SUBTYPE_PROPERTY);

    // get all source tracks
    String configuredSourceFlavors = workflowInstance.getCurrentOperation().getConfiguration(SOURCE_FLAVOR_PROPERTY);
    List<Track> sourceTracksList = new LinkedList<Track>();
    for (String f : StringUtils.split(configuredSourceFlavors, ",")) {
      String sourceFlavorStr = StringUtils.trimToNull(f);
      if (sourceFlavorStr == null)
        continue;
      MediaPackageElementFlavor sourceFlavor = MediaPackageElementFlavor.parseFlavor(sourceFlavorStr);
      String sourceFlavorType = sourceFlavor.getType();
      // Support for presenter/source*, presentation/source*
      String sourceFlavorSubtype = sourceFlavor.getSubtype();
      boolean wildcard = false;
      if (sourceFlavorSubtype.endsWith("*")) {
        sourceFlavorSubtype = sourceFlavorSubtype.substring(0, sourceFlavorSubtype.length() - 1);
        wildcard = true;
      }
      for (Track t : mp.getTracks()) {
        if ((t.getFlavor().getType().equals(sourceFlavorType) || "*".equals(sourceFlavorType))
                && (t.getFlavor().getSubtype().equals(sourceFlavorSubtype) || "*".equals(sourceFlavorSubtype) || (wildcard && t
                        .getFlavor().getSubtype().startsWith(sourceFlavorSubtype)))) {
          sourceTracksList.add(t);
        }
      }
    }
    if (sourceTracksList.size() == 0)
      throw new WorkflowOperationException(String.format(
              "No source tracks found. Configuration property %s is not valid", SOURCE_FLAVOR_PROPERTY));

    for (Track t : sourceTracksList) {
      Track clonedTrack = (Track) t.clone();
      clonedTrack.setIdentifier(null);
      clonedTrack.setURI(t.getURI()); // use the same URI as the original
      clonedTrack.setFlavor(new MediaPackageElementFlavor(t.getFlavor().getType(), configuredTargetFlavorSubtype));
      mp.addDerived(clonedTrack, t);
    }
    return createResult(mp, Action.SKIP);
  }

  public void setSmilService(SmilService smilService) {
    this.smilService = smilService;
  }

  public void setVideoEditorService(VideoEditorService editor) {
    this.videoEditorService = editor;
  }

  public void setWorkspace(Workspace workspace) {
    this.workspace = workspace;
  }
}
