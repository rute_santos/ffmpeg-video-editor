/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.videoeditor.ffmpeg;

import org.opencastproject.util.IoSupport;
import org.opencastproject.util.StreamHelper;
import org.opencastproject.videoeditor.impl.VideoClip;

import junit.framework.Assert;
import java.io.File;
import java.net.URISyntaxException;
import org.junit.Test;
import org.junit.Before;
import org.junit.BeforeClass;
import java.util.ArrayList; 
import org.slf4j.LoggerFactory;

/**
 *
 * @author pmiller
 */
public class FFmpegTest {
  
  /** The logging instance */
  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(FFmpegTest.class);
  private static final String FFMPEG_BINARY = "ffmpeg";
  public static final int WAIT_SEC = 3;
  
  protected String audioFilePath;
  protected String videoFilePath;
  protected String muxedFilePath;
  protected String smallFilePath;
  protected String outputFilePath;
  protected static boolean ffmpegInstalled = true;
    
  public FFmpegTest() {   // Economize! use same test files as gstreamer
    try {
      testForFFmpeg();
      audioFilePath = new File(getClass().getResource("/testresources/testvideo-a.mp4").toURI()).getAbsolutePath();
      videoFilePath = new File(getClass().getResource("/testresources/testvideo-v.mp4").toURI()).getAbsolutePath();
      muxedFilePath = new File(getClass().getResource("/testresources/testvideo.mp4").toURI()).getAbsolutePath();
      smallFilePath = new File(getClass().getResource("/testresources/BigBuckBunny_320x180.mp4").toURI()).getAbsolutePath();
      outputFilePath = new File("target/testoutput/mux.mp4").getAbsolutePath();
    } catch (URISyntaxException ex) {
      logger.error(ex.getMessage());
    }
  }
  
  @BeforeClass
  public static void testForFFmpeg() {
    StreamHelper stdout = null;
    StreamHelper stderr = null;
    Process p = null;
    // This code is taken from composer - not sure how to read the config file to do these tests
    try {
      p = new ProcessBuilder(FFMPEG_BINARY, "-version").start();
      stdout = new StreamHelper(p.getInputStream());
      stderr = new StreamHelper(p.getErrorStream());
      int status = p.waitFor();
      stdout.stopReading();
      stderr.stopReading();
      if (status != 0)
        throw new IllegalStateException();
    } catch (Throwable t) {
      logger.warn("Skipping image composer service tests due to unsatisifed or erroneus ffmpeg installation");
      ffmpegInstalled = false;
    } finally {
      IoSupport.closeQuietly(stdout);
      IoSupport.closeQuietly(stderr);
      IoSupport.closeQuietly(p);
    }
  }
  
  
  @Before
  public void setUp() {
    if (new File(outputFilePath).exists())  {
      new File(outputFilePath).delete();
    } else if (!new File(outputFilePath).getParentFile().exists()) {
      new File(outputFilePath).getParentFile().mkdir();
    }
  }
  

  /**
   * Test if ffmpeg can split and join 1 file
   * @return true if ffmpeg installed, false otherwise
   */
  @Test
  public void ffmpegEditTest() throws Exception {
   
          ArrayList<String> input = new ArrayList<String>();
          ArrayList<VideoClip> clips = new ArrayList<VideoClip>();
          clips.add(new VideoClip(0, 0.0, 10.0));
          clips.add(new VideoClip(0, 25.0, 44.0));
          input.add(muxedFilePath);
          if (ffmpegInstalled) {
            FFmpegEdit fmp = new FFmpegEdit();
            fmp.processEdits(input, outputFilePath, clips);
            Assert.assertTrue(new File(outputFilePath).exists());
            logger.info("Ffmpeg concat 2 clips from 1 file OK!"); 
          }
     
  }

  /**
   * Test if ffmpeg can split and join 2 files
   * @return true if ffmpeg installed, false otherwise
   */
  @Test
  public void ffmpegEditTest2Sources() throws Exception {
   
          ArrayList<String> input = new ArrayList<String>();
          ArrayList<VideoClip> clips = new ArrayList<VideoClip>();
          clips.add(new VideoClip(0, 0.0, 10.0));
          clips.add(new VideoClip(1, 25.0, 44.0));
          input.add(muxedFilePath);
          input.add(smallFilePath);  // different size,framerate
          if (ffmpegInstalled) {
            FFmpegEdit fmp = new FFmpegEdit();
            fmp.processEdits(input, outputFilePath, clips);
            Assert.assertTrue(new File(outputFilePath).exists());
            logger.info("Ffmpeg concat 2 clips from 2 files OK!"); 
          }
     
  }
}
