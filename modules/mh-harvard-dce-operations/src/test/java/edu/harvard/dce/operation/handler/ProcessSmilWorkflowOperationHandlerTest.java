/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package edu.harvard.dce.operation.handler;

import org.opencastproject.job.api.Job;
import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.mediapackage.MediaPackageBuilder;
import org.opencastproject.mediapackage.MediaPackageBuilderFactory;
import org.opencastproject.mediapackage.MediaPackageElementParser;
import org.opencastproject.mediapackage.Track;
import org.opencastproject.serviceregistry.api.ServiceRegistry;
import org.opencastproject.smil.api.SmilResponse;
import org.opencastproject.smil.api.SmilService;
import org.opencastproject.smil.entity.api.Smil;
import org.opencastproject.videoeditor.api.VideoEditorService;
import org.opencastproject.workflow.api.WorkflowInstance.WorkflowState;
import org.opencastproject.workflow.api.WorkflowInstanceImpl;
import org.opencastproject.workflow.api.WorkflowOperationInstance;
import org.opencastproject.workflow.api.WorkflowOperationInstance.OperationState;
import org.opencastproject.workflow.api.WorkflowOperationInstanceImpl;
import org.opencastproject.workflow.api.WorkflowOperationResult;
import org.opencastproject.workspace.api.Workspace;

import de.schlichtherle.io.File;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProcessSmilWorkflowOperationHandlerTest {

  private ProcessSmilWorkflowOperationHandler operationHandler;

  private MediaPackage mp;
  private MediaPackage mpUpdated;
  private Job jobPresenter;
  private Job jobPresentation;

  private SmilService smilService = null;
  private VideoEditorService videoEditorService = null;
  private Workspace workspace = null;

  @Before
  public void setUp() throws Exception {
    MediaPackageBuilder builder = MediaPackageBuilderFactory.newInstance().newMediaPackageBuilder();

    // test resources
    URI uriMP = ProcessSmilWorkflowOperationHandler.class.getResource("/smil/mediapackage.xml").toURI();
    URI uriMPUpdated = ProcessSmilWorkflowOperationHandler.class.getResource("/smil/mediapackage_updated.xml").toURI();
    URI uriSmil = ProcessSmilWorkflowOperationHandler.class.getResource("/smil/smil.xml").toURI();
    mp = builder.loadFromXml(uriMP.toURL().openStream());
    mpUpdated = builder.loadFromXml(uriMPUpdated.toURL().openStream());
    URI uriPresenterTrimmed = mpUpdated.getTrack("presenter-trimmed-id").getURI();
    URI uriPresentationTrimmed = mpUpdated.getTrack("presentation-trimmed-id").getURI();
    File smilFile = new File(uriSmil);

    // set up mock workspace
    workspace = EasyMock.createNiceMock(Workspace.class);
    EasyMock.expect(workspace.get(new URI("smil.xml"))).andReturn(smilFile);
    EasyMock.expect(
            workspace.moveTo(uriPresenterTrimmed, "process-smil-media-package-id", "presenter-trimmed-id",
                    "presenter_trimmed.mp4")).andReturn(uriPresenterTrimmed);
    EasyMock.expect(
            workspace.moveTo(uriPresentationTrimmed, "process-smil-media-package-id", "presentation-trimmed-id",
                    "presentation_trimmed.mp4")).andReturn(uriPresentationTrimmed);
    EasyMock.replay(workspace);

    // set up mock smil service
    Smil smil = EasyMock.createNiceMock(Smil.class);
    EasyMock.expect(smil.getId()).andReturn("smil-id");
    EasyMock.replay(smil);
    SmilResponse smilRsp = EasyMock.createNiceMock(SmilResponse.class);
    EasyMock.expect(smilRsp.getSmil()).andReturn(smil);
    EasyMock.replay(smilRsp);
    smilService = EasyMock.createNiceMock(SmilService.class);
    EasyMock.expect(smilService.fromXml(smilFile)).andReturn(smilRsp);
    EasyMock.replay(smilService);

    // set up mock jobs
    jobPresenter = EasyMock.createNiceMock(Job.class);
    EasyMock.expect(jobPresenter.getId()).andReturn(1l);
    EasyMock.expect(jobPresenter.getPayload())
            .andReturn(MediaPackageElementParser.getAsXml(mpUpdated.getTrack("presenter-trimmed-id"))).anyTimes();
    EasyMock.expect(jobPresenter.getStatus()).andReturn(Job.Status.FINISHED);
    EasyMock.expect(jobPresenter.getDateCreated()).andReturn(new Date());
    EasyMock.expect(jobPresenter.getDateStarted()).andReturn(new Date());
    EasyMock.expect(jobPresenter.getQueueTime()).andReturn(new Long(0));
    EasyMock.replay(jobPresenter);

    jobPresentation = EasyMock.createNiceMock(Job.class);
    EasyMock.expect(jobPresentation.getId()).andReturn(2l);
    EasyMock.expect(jobPresentation.getPayload())
            .andReturn(MediaPackageElementParser.getAsXml(mpUpdated.getTrack("presentation-trimmed-id"))).anyTimes();
    EasyMock.expect(jobPresentation.getStatus()).andReturn(Job.Status.FINISHED);
    EasyMock.expect(jobPresentation.getDateCreated()).andReturn(new Date());
    EasyMock.expect(jobPresentation.getDateStarted()).andReturn(new Date());
    EasyMock.expect(jobPresentation.getQueueTime()).andReturn(new Long(0));
    EasyMock.replay(jobPresentation);

    List<Job> jobs = new ArrayList<Job>();
    jobs.add(jobPresenter);
    jobs.add(jobPresentation);

    // set up mock video editor service
    videoEditorService = EasyMock.createNiceMock(VideoEditorService.class);
    EasyMock.expect(videoEditorService.processSmil(smil)).andReturn(jobs);
    EasyMock.replay(videoEditorService);

    // set up mock service registry
    ServiceRegistry serviceRegistry = EasyMock.createNiceMock(ServiceRegistry.class);
    EasyMock.expect(serviceRegistry.getJob(1l)).andReturn(jobPresenter);
    EasyMock.expect(serviceRegistry.getJob(2l)).andReturn(jobPresentation);
    EasyMock.replay(serviceRegistry);

    // set up service
    operationHandler = new ProcessSmilWorkflowOperationHandler();
    operationHandler.setWorkspace(workspace);
    operationHandler.setServiceRegistry(serviceRegistry);
    operationHandler.setSmilService(smilService);
    operationHandler.setVideoEditorService(videoEditorService);
  }

  @Test
  public void testStart() throws Exception {

    // operation configuration
    Map<String, String> configurations = new HashMap<String, String>();
    configurations.put("source-flavors", "*/source*");
    configurations.put("target-flavor-subtype", "trimmed");
    configurations.put("smil-flavor", "smil/smil");

    // add the mediapackage to a workflow instance
    WorkflowInstanceImpl workflowInstance = new WorkflowInstanceImpl();
    workflowInstance.setId(1);
    workflowInstance.setState(WorkflowState.RUNNING);
    workflowInstance.setMediaPackage(mp);
    WorkflowOperationInstanceImpl operation = new WorkflowOperationInstanceImpl("op", OperationState.RUNNING);
    operation.setTemplate("process-smil");
    operation.setState(OperationState.RUNNING);
    for (String key : configurations.keySet()) {
      operation.setConfiguration(key, configurations.get(key));
    }

    List<WorkflowOperationInstance> operationsList = new ArrayList<WorkflowOperationInstance>();
    operationsList.add(operation);
    workflowInstance.setOperations(operationsList);

    // Run the media package through the operation handler
    WorkflowOperationResult result = operationHandler.start(workflowInstance, null);

    // check track metadata
    MediaPackage mpNew = result.getMediaPackage();
    Track presenterTrimmed = mpNew.getTrack("presenter-trimmed-id");
    Assert.assertEquals("presenter/trimmed", presenterTrimmed.getFlavor().toString());
    Track presentationTrimmed = mpNew.getTrack("presentation-trimmed-id");
    Assert.assertEquals("presentation/trimmed", presentationTrimmed.getFlavor().toString());
  }

}
