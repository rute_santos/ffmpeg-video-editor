/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.videoeditor.silencedetection.ffmpeg;

import org.opencastproject.videoeditor.api.ProcessFailedException;
import org.opencastproject.videoeditor.ffmpeg.FFmpegEdit;
import org.opencastproject.videoeditor.ffmpeg.FFmpegTest;
import org.opencastproject.videoeditor.silencedetection.api.MediaSegment;
import org.opencastproject.videoeditor.silencedetection.api.MediaSegments;
import org.opencastproject.videoeditor.silencedetection.impl.SilenceDetectionProperties;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

/**
 *
 * Adapted from the gstreamer version
 */
public class SilenceDetectorTest extends FFmpegTest {
  
  private static final Logger logger = LoggerFactory.getLogger(SilenceDetectorTest.class);
  
  
  @BeforeClass
  public static void setUpClass() throws Exception {
    testForFFmpeg();
  }
  
  @Test
  public void detectorTest() {
    if (!ffmpegInstalled) return;
    logger.info("segmenting audio file '{}'...", audioFilePath);
    try {
      FFmpegEdit ffmpeg = new FFmpegEdit();
      
      MediaSegments segments = ffmpeg.runSilenceDetection(new Properties(), "track-1", audioFilePath);
      Assert.assertNotNull(segments);
      Assert.assertTrue(segments.getMediaSegments().size() > 0);
      
      logger.info("segments found:");
      for (MediaSegment segment : segments.getMediaSegments()) {
        
        logger.info("{}  - {}", new String[] {   
          Long.toString(segment.getSegmentStart()),
          Long.toString(segment.getSegmentStop())
        });
        
      }
      
    } catch (ProcessFailedException ex) {
      Assert.fail();
    }
  }
  
  @Test
  public void detectorSingleSegmentTest() {
    if (!ffmpegInstalled) return;
    
    logger.info("segmenting audio file '{}' with minimum silence length of 30 sec...", audioFilePath);
    
    Properties properties = new Properties();
    properties.setProperty(SilenceDetectionProperties.SILENCE_MIN_LENGTH, "30");
//    properties.setProperty(VideoEditorProperties.SILENCE_THRESHOLD_DB, "-75");
    
    try {
      FFmpegEdit ffmpeg = new FFmpegEdit();
      MediaSegments segments = ffmpeg.runSilenceDetection(new Properties(), "track-1", audioFilePath);
      Assert.assertNotNull(segments);
      logger.info("#segments found:" + segments.getMediaSegments().size());
      Assert.assertTrue(segments.getMediaSegments().size() == 1); 
      MediaSegment segment = segments.getMediaSegments().get(0);
      Assert.assertTrue(segment.getSegmentStart() < segment.getSegmentStop());
      logger.info("segments found:");
      logger.info("{}  - {}", new String[] {
        
        Long.toString(segment.getSegmentStart()),
        
        Long.toString(segment.getSegmentStop())
      });
            
    } catch (ProcessFailedException ex) {
      Assert.fail();
    }
  }
  
  @Test
  public void detectorFailTest() {
    if (!ffmpegInstalled) return;
    
    logger.info("segmenting video only file '{}' should fail...", videoFilePath);
    try {
      FFmpegEdit ffmpeg = new FFmpegEdit();     
      MediaSegments segments = ffmpeg.runSilenceDetection(new Properties(), "track-1", videoFilePath);
      //Assert.assertNull(segments);
      Assert.fail();
      
    } catch (ProcessFailedException ex) {
      logger.debug(ex.getMessage());
    }
  }
  
}
